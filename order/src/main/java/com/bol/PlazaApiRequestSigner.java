package com.bol;

import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

/**
 * @author jwalgemoed
 *
 * Simple helper class to sign a request to the V1 or V2 API for Bol plaza. Replace the public and private
 * key with the correct values to verify if it works.
 *
 * This method works with the V1 and V2 versions of the API.
 *
 * Verified with:
 *
 *  - Oracle JDK 1.8.0_66
 */
public class PlazaApiRequestSigner {

    public static final String X_BOL_DATE_HEADER = "X-BOL-Date";
    public static final String X_BOL_AUTHORIZATION_HEADER = "X-BOL-Authorization";
    public static final String ACCEPT_HEADER = "Accept";
    public static final String CONTENT_TYPE_HEADER = "Content-Type";

    /**
     * Insert public and private keys for the API here (NOT the TEST keys, but the keys for PRO as the test case
     * Below assumes a connection to open orders on PRO
     */
    private static final String PRIVATE_KEY = "86E80F142445D92C0DF16D9765D2821CE84D0C180C88761111BEDCFE6EB76E7E1DFDDCF16857A861DEC554A2E7E21D8ECAF1CE2DF5BE913838D1057E89F805B01736187BF3657DDFF20043D62D22B80386C8124696F1565E197193EFA6AE844927514A1FC56EE93CB61AC07030E289FA694D08A985C2136683444DF31C6E6CFE";
    private static final String PUBLIC_KEY = "7DFFE777140344A39EF119B6E4DAB479";
    private static final String MAC_TYPE = "HmacSHA256";

    /**
     * Sets the headers for the request we are attempting to do
     *
     * @param method      the request method (GET, POST, etc...)
     * @param path        the path for the request without the host (/services/rest/...)
     * @param contentType the content type for the request
     * @return an HttpHeaders instance filled with the required headers for the request
     * @throws Exception if anything goes wrong, particularly with the enconding
     */
    public static Map<String, String> createBolHeaders(final String method, final String path, final String contentType) throws Exception {
        final String dateString = formatDate(new Date());
        final String signature = getAuthenticationSignature(method, path, contentType, dateString);
        final Map<String, String> headers = new HashMap<>();

        headers.put(CONTENT_TYPE_HEADER, contentType);
        headers.put(ACCEPT_HEADER, contentType);
        headers.put(X_BOL_DATE_HEADER, dateString);
        headers.put(X_BOL_AUTHORIZATION_HEADER, PUBLIC_KEY + ":" + signature);

        return headers;
    }

    /**
     * Create the HMAC signature for the request.
     *
     * @param method      the request method (GET, POST, etc...)
     * @param path        the path for the request without the host (/services/rest/...)
     * @param dateString  the current date which is checked server side
     * @param contentType the content type for the request
     * @return the singnature string for this request.
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     */
    private static String getAuthenticationSignature(final String method, final String path, final String contentType, final String dateString) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        final String result = method + "\n\n"
                + contentType + "\n"
                + dateString + "\n"
                + X_BOL_DATE_HEADER.toLowerCase() + ":" + dateString + "\n"
                + path;

        final String privateKeyPlusSellerId = PRIVATE_KEY;
        final Mac sha256_HMAC = Mac.getInstance(MAC_TYPE);
        final SecretKeySpec secret_key = new SecretKeySpec(privateKeyPlusSellerId.getBytes(), MAC_TYPE);
        sha256_HMAC.init(secret_key);

        Base64.Encoder b = Base64.getEncoder();
        return  b.encodeToString(sha256_HMAC.doFinal(result.getBytes("UTF-8")));
    }

    /**
     * Formats a date in the correct format for interpretation on the server side.
     *
     * @param date the date to format
     * @return the formatted date
     */
    private static String formatDate(final Date date) {
        final String ISO_FORMAT = "EEE, dd MMM YYYY HH:mm:ss zzz";
        final TimeZone utc = TimeZone.getTimeZone("GMT");
        final SimpleDateFormat isoFormatter = new SimpleDateFormat(ISO_FORMAT);
        isoFormatter.setTimeZone(utc);
        return isoFormatter.format(date);
    }

    /**
     * Simple main method to show a usage example for this class
     */
}