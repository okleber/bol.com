package com.bol;

import java.util.Map;

public class Main {
    public static void main(final String[] args) throws Exception {
        // Http method for the operation
        final String httpMethod = "GET";
        // Path for the request - must match the exact path of the request including parameters
        final String requestPath = "/services/rest/orders/v2";
        // The content_type for the request (sets the accept header too)
        final String contentType = "application/xml";

        // Call the header generation and use these to execute the http request to the resource. Returns
        // a map containing the correct headers.
        // Please note: signing uses the current date which is checked server side. Generate this header
        // information right before doing the call - or you may get access denied anyway!
        //System.out.println(PlazaApiRequestSigner.createBolHeaders(httpMethod, requestPath, contentType));
        Map<String,String> m = PlazaApiRequestSigner.createBolHeaders(httpMethod, requestPath, contentType);

        new JavaGetRequest(m);
    }
    }

