package com.bol;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class JavaGetRequest {

    private static HttpURLConnection con;

    JavaGetRequest(Map<String, String> m) throws MalformedURLException,
            ProtocolException, IOException {

        String url = "https://plazaapi.bol.com/services/rest/orders/v2";
        //String urlParameters = "page=1&fulfilment-method=FBR";
        //byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);



            URL myurl = new URL(url);
            HttpURLConnection con =(HttpURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("GET");
            /*con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("X-BOL-Authorization",m.get("X-BOL-Authorization"));
            con.setRequestProperty("X-BOL-Date",m.get("X-BOL-Date"));

            Map<String, List<String>> x = con.getRequestProperties();
            for (Map.Entry<String,List<String>> ee: x.entrySet()){
                System.out.println(ee.getKey()+":"+ee.getValue());
            }*/

            for (Map.Entry<String, String> entry : m.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                System.out.println(key+":"+value);
                con.setRequestProperty(key, value);
            }

            con.connect();

            try{
                InputStream in = con.getInputStream();
            }catch (IOException ii){
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream())) ;
                String line;
                StringBuilder content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }

                System.out.println(content.toString());
                ii.printStackTrace();
            }
        }
    }
